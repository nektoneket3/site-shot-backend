const express = require('express');

const validate =        require('../shared/validation');
const validationRules = require('./auth.validation-rules');
const AuthController =  require('./auth.controller');


const router = express.Router();
const authController = new AuthController();

router.post('/signup', validate(validationRules.login), authController.signup);
router.post('/login', validate(validationRules.login), authController.login);

module.exports = router;
