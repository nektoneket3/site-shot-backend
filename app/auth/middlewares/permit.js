module.exports = function permittedFor(role) {
  return (req, res, next) => {
    if (req.user.role === role) {
      return next();
    }
    res.status(403).json('Forbidden');
  };
};

module.exports = function notPermittedFor(role) {
  return (req, res, next) => {
    if (req.user.role === role) {
      return res.status(403).json('Forbidden');
    }
    next();
  };
};
