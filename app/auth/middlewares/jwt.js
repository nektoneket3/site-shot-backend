const jwt =   require('jsonwebtoken');
const get =   require('lodash/get');
const User =  require('../../users/user.model');
const roles = require('../roles');

const guest = {
  role: roles.guest,
  isGuest() {
    return true 
  }
};

// TODO REFACTOR IT
module.exports = async function checkToken(req, res, next) {
  // JWT middleware
  const [prefix, token] = get(req, 'headers.authorization', '').split(' ');

  req.user = guest;

  if (prefix === 'JWT') {
    await jwt.verify(token, process.env.SECRET_KEY, async (error, decodedUser) => {
      if (!error) {
        const user = await User.findOne({ email: decodedUser.email });
        if (user) {
          req.user = user;
        }
      }
    })
  }
  next();
};
