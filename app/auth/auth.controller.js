const bcrypt           = require('bcryptjs');
const jwt              = require('jsonwebtoken');
const httpCodes        = require('http-status-codes');

const User             = require('../users/user.model');
const roles            = require('./roles');
const { unauthorized } = require('../shared/helpers/response');


module.exports = class AuthController {

  async signup(req, res, next) {
    const user = new User({
      fullName: req.body.fullName || null,
      email: req.body.email,
      hashPassword: bcrypt.hashSync(req.body.password, 10),
      role: roles.customer
    });

    res.status(httpCodes.CREATED).json(
      await user.save().catch(error => next(error))
    );
  }

  async login(req, res) {
    const user = await User.findOne({ email: req.body.email });

    if (!user) {
      return unauthorized(res, { messages: ['Email not registered'], field: 'email' });
    }

    if (!user.comparePassword(req.body.password)) {
      return unauthorized(res, { messages: ['Wrong password'], field: 'password' });
    }

    const token = jwt.sign(
      {
        email: user.email,
        _id: user._id
      },
      process.env.SECRET_KEY
    );

    res.json({ token, user });
  }
};
