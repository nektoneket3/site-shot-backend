const http = require('http');


module.exports = function(url) {
  return new Promise((resolve) => {
    http.get(url, (res) => {
      resolve(res.connection.remoteAddress);
    });
  })
};


