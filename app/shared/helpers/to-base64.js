const fs = require('fs');


module.exports = function toBase64(path) {
  return `data:image/png;base64,${fs.readFileSync(`.${path}`, 'base64')}`
};
