const puppeteer = require('puppeteer');

const toBase64 = require('./to-base64');


module.exports = async function screenshot(url, savePath) {
  let browser;

  try {
    browser = await puppeteer.launch({
      executablePath: '/usr/bin/chromium-browser',
      args: ['--disable-dev-shm-usage', '--no-sandbox', '--disable-setuid-sandbox']
    });
    const page = await browser.newPage();
    await page.setViewport({ width: 1024, height: 800 });
    await page.goto(url);
    await page.screenshot({ path: `.${savePath}` });
    return toBase64(savePath);
  }
  catch (err) {
    console.log(err);
    throw new Error('Cannot make a screenshot of the given URL');
  }
  finally {
    if (browser) {
      await browser.close();
    }
  }
};
