module.exports = function escape(regexString) {
  return regexString.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
};
