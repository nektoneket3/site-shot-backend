const httpCodes = require('http-status-codes');
const { isString, isArray } = require('lodash');

const isObject = require('./is-object');


class ErrorResponse {
  constructor(error) {
    if (isString(error)) {
      this.errors = [{ messages: [error] }];

    } else if (isArray(error)) {
      this.errors = error

    } else if (isObject(error)) {
      this.errors = [error]

    } else {
      this.errors = error
    }
  }
}

function errorResponse(res, error, status = httpCodes.BAD_REQUEST) {
  res.status(status).json(new ErrorResponse(error));
}

function badRequest(res, error) {
  errorResponse(res, error, httpCodes.BAD_REQUEST);
}

function internalError(res, error) {
  errorResponse(res, error, httpCodes.INTERNAL_SERVER_ERROR);
}

function unauthorized(res, error) {
  errorResponse(res, error, httpCodes.UNAUTHORIZED);
}

module.exports = {
  errorResponse,
  badRequest,
  internalError,
  unauthorized
};
