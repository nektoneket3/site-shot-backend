const multer = require('multer');
const config = require('../config');


module.exports = function fileUpload(storageOptions = {}) {

  const defaultStorageOptions = {
    destination(req, file, cb) {
      cb(null, `.${config.uploadsDir}`)
    },
    filename(req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}-${file.originalname}`);
    }
  };

  return multer({ storage: multer.diskStorage(
    { ...defaultStorageOptions, ...storageOptions }
  )});
};
