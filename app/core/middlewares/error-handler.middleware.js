const validate = require('express-validation');
const reduce   = require('lodash/reduce');

const {
  badRequest,
  internalError
} = require('../../shared/helpers/response');


module.exports = function errorHandlerMiddleware(error, req, res, next) {
  if (typeof (error) === 'string') {
    // custom application error
    return badRequest(res, error);
  }

  if (error instanceof validate.ValidationError) {
    // send validation errors in another format
    return badRequest(res, error.errors);
  }

  if (error.name === 'MongoError') {
    // send mongo errors in another format
    return badRequest(res, error.errmsg);
  }

  if (error.name === 'ValidationError') {
    // send mongo errors in another format
    return badRequest(res, reduce(
      error.errors,
      (acc, { path, message }) => [...acc, {
        field: path,
        messages: [message]
      }],
      []
    ));
  }

  // default to 500 server error
  return internalError(res, error.message);
};
