const express =    require('express');
const bodyParser = require('body-parser');
const cors =       require('cors');
const path =       require('path');
const config =     require('./config');


const app = express();

app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json());
app.use(cors());
app.use(config.uploadsUrl, express.static(path.join(__dirname, `..${config.uploadsDir}`)));

app.use(`${config.apiPrefix}`,        require('./auth/middlewares/jwt'));

app.use(`${config.apiPrefix}/auth`,   require('./auth/auth.routes'));
app.use(`${config.apiPrefix}/users`,  require('./users/users.routes'));
app.use(`${config.apiPrefix}/sites`,  require('./sites/sites.routes'));

app.use(`${config.apiPrefix}`,        require('./core/middlewares/error-handler.middleware'));

module.exports = app;
