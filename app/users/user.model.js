const mongoose        = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt          = require('bcryptjs');

const roles           = require('../auth/roles');


const userSchema = new mongoose.Schema({
  fullName: {
    type: String,
    trim: true,
    required: false,
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    required: true
  },
  hashPassword: {
    type: String,
    required: true
  },
  fullAvatar: {
    type: String,
    required: false,
    default: null
  },
  minAvatar: {
    type: String,
    required: false,
    default: null
  },
  created: {
    type: Date,
    default: Date.now
  },
  role: {
    type: String,
    default: roles.customer
  },
  settings: {
    type: Object,
    default: {}
  }
});

userSchema.plugin(uniqueValidator);

userSchema.methods.comparePassword = function(password) {
  return bcrypt.compareSync(password, this.hashPassword)
};

userSchema.methods.isGuest = function() {
  return false;
};

module.exports = mongoose.model('User', userSchema);
