const express =         require('express');

const fileUpload =      require('../shared/file-upload');
const UsersController = require('./users.controller');
const permittedFor =    require('../auth/middlewares/permit');
const notPermittedFor = require('../auth/middlewares/permit');
const roles =           require('../auth/roles');


const router = express.Router();
const usersController = new UsersController();

router.get('/', permittedFor(roles.admin), usersController.list);

router.get('/current', usersController.getCurrentUser);

router.patch('/current', notPermittedFor(roles.guest), usersController.update.bind(usersController));

router.patch('/current/avatar',
  notPermittedFor(roles.guest),
  fileUpload().fields(usersController.userAvatarFields),
  usersController.updateUserAvatar.bind(usersController)
);

module.exports = router;
