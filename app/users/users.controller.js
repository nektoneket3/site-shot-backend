const User =       require('./user.model');
const isEmpty =    require('lodash/isEmpty');


module.exports = class UsersController {

  constructor() {
    this.userAvatarFields = [
      { name: 'fullAvatar', maxCount: 1 },
      { name: 'minAvatar', maxCount: 1 }
    ]
  }

  getCurrentUser(req, res) {
    res.json(req.user);
  }

  async list(req, res) {
    res.json(await User.find());
  }

  async updateUserAvatar(req, res, next) {
    if (isEmpty(req.files)) {
      return next('No files were uploaded.');
    }

    const fields = {
      fullAvatar: `/${req.files.fullAvatar[0].path}`,
      minAvatar: `/${req.files.minAvatar[0].path}`,
    };

    const updatedUser = await this._updateUser(req.user._id, fields);

    res.json(updatedUser);
  }

  async update(req, res) {
    const updatedUser = await this._updateUser(req.user._id, req.body);
    res.json(updatedUser);
  }

  _updateUser(userId, fields) {
    return User.findByIdAndUpdate(
      userId,
      { $set: fields },
      { new: true, useFindAndModify: false }
    );
  }
};
