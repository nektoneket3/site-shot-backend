const mongoose =         require('mongoose');
const mongoosePaginate = require('mongoose-paginate');


const Schema = mongoose.Schema;

const siteSchema = new Schema({
  screenShot: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  },
  origin: {
    type: String,
    default: ''
  },
  protocol: {
    type: String,
    default: ''
  },
  host: {
    type: String,
    default: ''
  },
  hostName: {
    type: String,
    default: ''
  },
  path: {
    type: String,
    default: ''
  },
  pathName: {
    type: String,
    default: ''
  },
  query: {
    string: String,
    dict: Object
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

siteSchema.plugin(mongoosePaginate);

siteSchema.index({ url: 'text' });

module.exports = mongoose.model('Site', siteSchema);
