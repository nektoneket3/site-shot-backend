const express = require('express');

const validate =        require('../shared/validation');
const validationRules = require('./sites.validation-rules');
const notPermittedFor = require('../auth/middlewares/permit');
const roles =           require('../auth/roles');
const SitesController = require('./sites.controller');


const router = express.Router();
const sitesController = new SitesController();

router.get('/', notPermittedFor(roles.guest), sitesController.list.bind(sitesController));
router.post('/', validate(validationRules.create), sitesController.create.bind(sitesController));


module.exports = router;
