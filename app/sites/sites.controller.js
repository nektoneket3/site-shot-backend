const url               = require('url');
const urlParse          = require('url-parse');
const httpCodes         = require('http-status-codes');
const isEmpty           = require('lodash/isEmpty');

const Site              = require('./site.model');
const config            = require('../config');
const screenshot        = require('../shared/helpers/screenshot');
const escape            = require('../shared/helpers/escape');
const { internalError } = require('../shared/helpers/response');


module.exports = class SitesController {

  constructor() {
    this.defaultQueryParams = {
      sortBy: 'created',
      sort: 'desc',
      limit: 5
    }
  }

  async list({ query, user }, res) {
    const sortBy = query.sortBy || this.defaultQueryParams.sortBy;
    const sort = query.sort || this.defaultQueryParams.sort;
    const limit = query.limit || this.defaultQueryParams.limit;
    const searchString = query.searchString;
    const queryObject = { user: user._id };

    if (searchString) {
      queryObject.url = new RegExp(escape(searchString), 'gi');
    }

    res.json(
      await Site.find(queryObject).sort({ [sortBy]: sort }).limit(limit)
    );
  }

  async create(req, res, next) {
    const site = {
      screenShot: null,
      ...this._getParsedUrl(req.body.url)
    };

    const screenName = req.user.isGuest()
      ? `guest-screen-shot-${Date.now()}.png`
      : `${req.user._id}-${site.url.replace(/[\/?]/g, '')}-${Date.now()}.png`;

    const screenPath = `${config.uploadsDir}${screenName}`;

    try {
      site.screenShot = await screenshot(site.url, screenPath);
    } catch (err) {
      return internalError(res, { messages: [err.message], field: 'screenShot' });
    }

    if (req.user.isGuest()) {
      return res.json(site);
    }

    const notSavedSite = new Site({
      ...site,
      ...{ screenShot: screenPath },
      user: req.user._id
    });

    notSavedSite.save()
      .then((savedSite) => {
        site._id = savedSite._id;
        res.status(httpCodes.CREATED).json(site);
      })
      .catch(error => next(error));
  }

  _getParsedUrl(urlString) {
    const parsedUrl = url.parse(urlString, true);
    const additionalParsedUrl = urlParse(urlString);
    return {
      url: parsedUrl.href,
      origin: additionalParsedUrl.origin,
      protocol: parsedUrl.protocol.replace(':', ''),
      host: parsedUrl.host,
      hostName: parsedUrl.hostname,
      path: parsedUrl.path,
      pathName: parsedUrl.pathname,
      query: {
        string: parsedUrl.search
          ? parsedUrl.search.replace(/\?/, '')
          : null,
        dict: !isEmpty(parsedUrl.query)
          ? parsedUrl.query
          : null
      },
    }
  }

  // listPaginate({ query }, res) {
  //   const params = {
  //     page: +query.page,
  //     limit: +query.limit,
  //     sort: { date: -1 },
  //   };
  //
  //   if (query.sort) {
  //     params.sort = JSON.parse(query.sort);
  //   }
  //
  //   Site.paginate({}, params, (error, sites) => {
  //     res.send(sites);
  //   });
  // }
};
