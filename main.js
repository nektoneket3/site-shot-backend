['env', 'db'].map((config) => {
  require(`./config/${config}`);
});

const app  = require('./app/app');
const http = require('http').Server(app);

http.listen(process.env.HTTP_PORT, () => {
  console.log(`Application is running`);
});
