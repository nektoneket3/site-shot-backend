const mongoose = require('mongoose');

mongoose.connect(
  `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`,
  { useNewUrlParser: true }
);

mongoose.connection.on('error', (error) => {
  console.error('MondoDB Connection Error', error);
});

module.exports = mongoose;
